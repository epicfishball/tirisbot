# tirisbot

Custom Discord bot for [Tirisgarde Project](https://tirisgarde.org/).

Built with [pydisbot](https://github.com/ztxle/pydisbot) on top of [discord.py](https://github.com/Rapptz/discord.py)

Requires Python 3.6 and above.


### Setup app config:

1. Open up `config.txt`. Here, you can define the various settings that are used in `main.py`. These settings can also be defined through environment variables (see step 4).
2. `GM_LOG_FILE` is one of these settings. Input the full path to the logfile. Don't use quotes. [`DISCORD_TOKEN`](https://discordapp.com/developers/applications/) is another required setting, but see below for notes on that.
3. You can define the other settings here but be careful not to expose sensitive settings (if you ever push into this public repo).
4. You can define environment variables in Command Prompt using `SET VARNAME=FOO`. Again, no quotes needed.
5. The app requires a [`DISCORD_TOKEN`](https://discordapp.com/developers/applications/) to be defined, either in `config.txt` or [environment variables](https://ss64.com/nt/set.html). This is NOT the same as an application secret. You have to **convert the application into a bot** to receive the bot token.

### Setup environment:

1. Ensure you have Python 3.6 and above. Run `python --version` to check.
2. Using Elevated Command Prompt, install [pipenv](https://pipenv.readthedocs.io/) using the command `pip install --user pipenv`. Ensure that your PATH variable points to the folder containing `pipenv.exe` (usually `<python install dir>\Scripts`).
3. Open a new Command Prompt window (you can close the Elevated one). Navigate to the app directory (where `main.py` is). Run `pipenv shell`, then `pipenv install`. This creates a virtual environment for the app to run, and installs the necessary packages.
4. Use `python main.py` to run the app. If all goes well, you will see a "logged in" message.

### Maintenance notes:

* Before running the app, make sure you are in the virtual environment, i.e. you run `pipenv shell` before `python main.py`.
* The app will run as long as the window is open. Use `Ctrl-C` to shutdown. Shutdown may take a few seconds and give you a bunch of ugly errors (that's normal).
* Environment variables defined via `SET` are lost when you close the Command Prompt window. For your convenience, consider making a [batch script](https://ss64.com/nt/syntax-run.html).

### Privileged commands and authentication:

* 'Privileged' commands can only be used by authenticated users.
* The default method to authenticate yourself is to direct message the bot `!auth XXXX`, where XXXX is the authcode (case-sensitive).
* You find see the `authcode` in the app's console output a few seconds after the login message. Only the latest authcode is valid.
* The authcode changes over time (default: every 5 minutes), after every successful authentication, or after 5 failed attempts to authenticate with the current code.
* The app generates a json file to keep track of authenticated users. This is called `privileges.json` by default. You can mess with it if you want text-editor control over authenticated users.

### Hidden commands:

* 'Hidden' commands are basically 'paranoid mode' privileged commands. 
* These commands only take effect when 1) the command is issued via direct message to the bot, and 2) the command user is authenticated.
* The bot will not respond to hidden commands from unauthorized users.


### Basic modification:

* The `bot.bind_command()` endpoint is the main way to attach commands that the bot listens to. It is designed to be used as a [decorator](https://dbader.org/blog/python-decorators).
* The function that is defined immediately after `@bot.bind_command()` should be a coroutine (defined via the keyword `async def`).
* The return value from your callback should be a `pydisbot.components.Response` object. It is automatically fed to `bot.dispatch()` via the `bind_command()` abstraction.
* You can manually dispatch responses using `await bot.dispatch(Response(msg='foo'))`. This can be useful if you want a command to return multiple messages (e.g. to deliver multiple files).
