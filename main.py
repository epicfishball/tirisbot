import os
import shutil

from pydisbot.bots import BasicDiscordBot
from pydisbot.auth import BadAuthCodeError
from pydisbot.components import Response
from pydisbot.config import read_config


config = read_config((
    ('DISCORD_TOKEN',   None,               str),
    ('AUTHFILE',        'privileges.json',  str),
    ('DEBUGGING',       False,              bool),
    ('GM_LOG_FILE',     None,               str),

), config_file='config.txt')

bot = BasicDiscordBot()
bot.setup_auth(config.AUTHFILE)

BASEDIR = os.path.abspath(os.path.dirname(__file__))


@bot.bind_command(keyword='auth', hidden=True)
async def auth_member(message, argstr):
    user = message.author
    if bot.privcheck.has_privilege(user):
        return Response(msg='You are already authenticated.')
    try:
        bot.privcheck.set_trust_for_userid(user.id, argstr)
        print(f'[auth] User {str(user)} authenticated with {argstr}')
        return Response(msg='Successfully authenticated.\nYou can now use '
                            'privileged commands.')
    except BadAuthCodeError:
        print(f'[auth] User {str(user)} failed with authcode {repr(argstr)}')
        return


@bot.bind_command(keyword='sendlog', privileged=True, hidden=False)
async def send_gm_log(message, argstr):
    logfile = config.GM_LOG_FILE
    lines = 0
    try:
        lines = int(argstr or 0)
    except ValueError:
        raise TypeError("arguments to this command must be numbers, not " +
                        repr(argstr))

    if not logfile:
        return Response(msg='No filepath to GM_LOG_FILE was defined.')
    
    try:
        # Check that logfile is reachable
        open(logfile, 'r').close()
        # Copy file to working dir
        cp = shutil.copy(logfile, BASEDIR)
        
        # Open file descriptor for file existence check
        with open(cp, 'r', encoding='utf-8') as f:
            # Return whole file if no lines specified
            if not lines:
                return Response(file=cp)
            else:         
                # If num of lines from bottom specified, figure out tempfile name
                # Save selected lines from the bottom in a var
                selected = f.readlines()[-lines:]

        # Iterate over the source file's filepath to find the filename
        tgtfile = ''
        for i in range(len(cp) - 1, -1, -1):
            if i in '/\\':
                break
            tgtfile += cp[i]
        # Append 'truncated_' to the front of the filename
        trunc_fp = 'truncated_' + tgtfile

        # Write the selected lines to 'truncated_' file
        with open(trunc_fp, 'w', encoding='utf-8') as f:
            f.write('\n'.join(selected))
        return Response(file=trunc_fp)

    except FileNotFoundError:
        return Response(msg=f'File not found:\n`{logfile}`')



if __name__ == '__main__':
    bot.start(config.DISCORD_TOKEN)
    pass
